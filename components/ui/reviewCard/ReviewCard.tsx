import Image from 'components/ui/image/Image'
import React, { useMemo } from 'react'
import { arrayFromNumber } from 'utils/helper'

interface ReviewCardProps {
    product: {
        url: string
        brandUrl: string
        img: string
        title: string
        brand: string
    }
    star: number
    publishedAt: string
    content: string | React.ReactNode
    reviewer: {
        img: string
        name: string
        skinType: string
    }
    readMoreUrl: string
    containerClassName?: string
}

const ReviewCard = (props: ReviewCardProps) => {
    const renderStar = useMemo(() => {
        const roundedDownStarAmount = Math.floor(props.star)
        const inActiveStarAmount = 5 - roundedDownStarAmount

        return (
            <>
                {
                    arrayFromNumber(roundedDownStarAmount).map((item, index) =>
                        <Image data-testid="test-review-card-filled-star" key={index} src="./img/star-filled.png" alt="star-filled" width="16px" height="16px" className="w-4 h-4" />
                    )
                }
                {
                    arrayFromNumber(inActiveStarAmount).map((item, index) =>
                        <Image data-testid="test-review-card-empty-star" key={index} src="./img/star.png" alt="star" width="16px" height="16px" className="w-4 h-4" />
                    )
                }
            </>
        )
    }, [props.star])

    return (
        <div data-testid="test-review-card" className={`w-full relative ${props.containerClassName || ''}`}>
            <div className="border-2 border-border-three px-3 pt-5 pb-8 rounded-lg">
                <div className="flex flex-row items-center pb-3 border-b boder-border-three">
                    <a href={props.product.url} className="block w-[70px] h-[70px] relative mr-3 overflow-hidden rounded-lg">
                        <Image src={props.product.img} alt="article" width="70px" height="70px" className="absolute inset-0 w-full h-full object-cover object-center image-hover" />
                    </a>
                    <div className="">
                        <a href={props.product.url} className="block">
                            <h3 className="text-base font-bold color-transition hover:text-primary">
                                {props.product.title}
                            </h3>
                        </a>
                        <a href={props.product.brandUrl} className="block">
                            <p className="font-medium leading-snug color-transition hover:text-primary">
                                {props.product.brand}
                            </p>
                        </a>
                    </div>
                </div>
                <div className="w-full">
                    <div className="w-full flex flex-row items-center mt-2 mb-3">
                        <div className="flex flex-row items-center mr-auto">
                            {renderStar}
                        </div>
                        <span className="text-subtitle-three text-[11px] opacity-50">
                            {props.publishedAt}
                        </span>
                    </div>
                    <p className="text-[13px] leading-tight pr-3">
                        <span>{props.content}</span>...&nbsp;<a href={props.readMoreUrl} className="text-primary font-medium">Read More</a>
                    </p>
                </div>
            </div>
            <div className="flex flex-col items-center justify-center absolute bottom-0 left-1/2 transform -translate-x-1/2 translate-y-[74px]">
                <Image src={props.reviewer.img} alt="reviewer" className="w-[48px] h-[48px] mb-1.5" />
                <p className="mb-0.5">
                    {props.reviewer.name}
                </p>
                <p className="text-[11px] text-subtitle">
                    {props.reviewer.skinType}
                </p>
            </div>
        </div>
    )
}

export default ReviewCard
