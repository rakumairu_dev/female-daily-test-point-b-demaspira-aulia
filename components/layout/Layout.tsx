import React from 'react'
import Footer from './Footer'
import Header, { Meta } from './Header'
import Nav from './Nav'

interface LayoutProps {
    children: React.ReactNode
    meta: Meta
}

const Layout = (props: LayoutProps) => {
    return (
        <>
            <Header {...props.meta} />
            <Nav />
            {props.children}
            <Footer />
        </>
    )
}

export default Layout
