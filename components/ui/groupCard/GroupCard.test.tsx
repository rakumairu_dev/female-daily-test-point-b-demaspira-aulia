import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import GroupCard from './GroupCard'


test('component did load', async () => {
    render(
        <GroupCard
            img='https://placeimg.com/300/300/any'
            title='group title'
            subtitle='group subtitle'
            url='#'
        />
    )

    expect(screen.getByTestId('test-group-card')).toBeInTheDocument()
    expect(screen.getByText('group title')).toBeInTheDocument()
    expect(screen.getByText('group subtitle')).toBeInTheDocument()
})
