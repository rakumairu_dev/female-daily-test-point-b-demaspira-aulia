import Axios from 'axios'

const api = Axios.create({
    baseURL: 'https://virtserver.swaggerhub.com/hqms/FDN-WP/0.1',
})

export default api
