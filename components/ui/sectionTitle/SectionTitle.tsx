import React, { useMemo } from 'react'

interface SectionTitleProps {
    title: string
    subtitle: string
    withSeeMore?: boolean
    seeMoreUrl?: string
}

const SectionTitle = (props: SectionTitleProps) => {
    const renderSubtitle = useMemo(() => (
        <sub data-testid='test-section-title-sub' className={`block text-xl text-subtitle ${props.withSeeMore ? '' : 'mb-8'}`}>
            {props.subtitle}
        </sub>
    ), [props.subtitle, props.withSeeMore])

    return (
        <>
            <h2 data-testid='test-section-title-h2' className="text-2xl font-bold tracking-tight poppins mb-1.5">
                {props.title}
            </h2>
            {
                props.withSeeMore ?
                    <div data-testid="test-section-title-see-more" className="w-full flex flex-row items-center justify-between mb-8">
                        {renderSubtitle}
                        <a href={props.seeMoreUrl || '#'} className="flex items-center text-xl text-primary hover:text-primary-dark color-transition link-with-caret">
                            See More <div className="caret ml-5 -mb-0.5"></div>
                        </a>
                    </div>
                    :
                    renderSubtitle
            }
        </>
    )
}

export default SectionTitle
