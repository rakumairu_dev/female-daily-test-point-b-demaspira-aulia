import Image from 'components/ui/image/Image'
import React from 'react'

interface GroupCardProps {
    url: string
    img: string
    title: string
    subtitle: string
}

const GroupCard = (props: GroupCardProps) => {
    return (
        <a href={props.url} data-testid="test-group-card" className="w-full border border-border rounded-lg shadow-lg hover:shadow-xl shadow-transition pt-3.5 px-6 pb-12 flex flex-col items-center">
            <div className="w-full px-5 mb-2.5">
                <div className="pt-[100%] relative w-full h-auto">
                    <Image src={props.img} alt="person" className="absolute inset-0 w-full h-full object-cover object-center" />
                </div>
            </div>
            <h3 className="text-[22px] font-bold mb-8">
                {props.title}
            </h3>
            <Image src="./img/popular-groups/bar.png" alt="bar" className="w-full h-auto px-3 mb-1" />
            <p className="text-sm text-center font-medium">
                {props.subtitle}
            </p>
        </a>
    )
}

export default GroupCard
