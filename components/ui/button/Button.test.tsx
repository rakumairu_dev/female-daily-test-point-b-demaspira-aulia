import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import Button from './Button'

const BUTTON_TEXT = 'Button Component'

test('component did load with correct text', async () => {
    render(<Button>{BUTTON_TEXT}</Button>)

    const renderedButtonText = (screen.getByText(BUTTON_TEXT) as HTMLButtonElement).innerHTML

    expect(renderedButtonText).toBe(BUTTON_TEXT)
})

test('button is an anchor element', async () => {
    render(<Button href='#'>{BUTTON_TEXT}</Button>)

    const renderedEl = screen.getByTestId('test-button') as HTMLElement
    
    expect(renderedEl.tagName).toBe('A')
})

test('button is a button element', async () => {
    render(<Button>{BUTTON_TEXT}</Button>)

    const renderedEl = screen.getByTestId('test-button') as HTMLElement
    
    expect(renderedEl.tagName).toBe('BUTTON')
})
