import Head from 'next/head'
import React from 'react'

export interface Meta {
    title: string
    description: string
    preconnect?: string[]
    preloadImages?: string[]
}

const Header = (meta: Meta) => {
    return (
        <Head>
            <meta charSet="UTF-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>{meta.title}</title>
            <meta name="title" content={meta.title} />
            <meta name="description" content={meta.description} />
            <meta property="og:site_name" content="Female Daily" />
            <meta property="og:url" content="femaledaily.com" />
            <meta property="og:title" content={meta.title} />
            <meta property="og:description" content={meta.description} />
            <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
        
            <link rel="canonical" href="/" />

            {
                meta.preconnect?.length > 0 &&
                meta.preconnect.map((url, index) => <link key={index} rel="preconnect" href={url} />)
            }

            {
                meta.preloadImages?.length > 0 &&
                meta.preloadImages.map((url, index) => <link key={index} rel="preload" href={url} as="image" />)
            }
        </Head>
    )
}

export default Header
