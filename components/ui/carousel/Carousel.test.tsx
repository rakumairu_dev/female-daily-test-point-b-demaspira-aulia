import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Carousel from './Carousel'

test('render 2 items with next button', () => {
    const { getAllByTestId, getByTestId } = render(
        <Carousel
            show={1}
        >
            <h2 data-testid="carousel-item-1">Item 1</h2>
            <h2 data-testid="carousel-item-2">Item 2</h2>
        </Carousel>
    )

    const items = getAllByTestId(/carousel-item-*/)
    const nextButton = getByTestId('test-carousel-next-button')

    expect(items).toHaveLength(2)
    expect(nextButton).toBeInTheDocument()
})

test('render 2 items with passive next button', () => {
    const { getAllByTestId, queryByTestId } = render(
        <Carousel
            show={3}
        >
            <h2 data-testid="carousel-item-1">Item 1</h2>
            <h2 data-testid="carousel-item-2">Item 2</h2>
        </Carousel>
    )

    const items = getAllByTestId(/carousel-item-*/)
    const nextButton = queryByTestId('test-carousel-next-button')

    expect(items).toHaveLength(2)
    expect(nextButton.className.includes('cursor-default')).toBeTruthy()
})