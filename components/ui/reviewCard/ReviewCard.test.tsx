import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import ReviewCard from './ReviewCard'

beforeEach(() => {
    render(
        <ReviewCard
            content="review content"
            product={{
                title: 'product title',
                brand: 'product brand',
                img: 'https://placeimg.com/300/300/any',
                url: '#',
                brandUrl: '#',
            }}
            publishedAt="2 hours ago"
            readMoreUrl='#'
            reviewer={{
                img: 'https://placeimg.com/300/300/any',
                name: 'reviewer name',
                skinType: 'reviewer skin type',
            }}
            star={3.6}
        />
    )
})

test('component did load', async () => {
    expect(screen.getByTestId('test-review-card')).toBeInTheDocument()
    expect(screen.getByText('review content')).toBeInTheDocument()
    expect(screen.getByText('product title')).toBeInTheDocument()
    expect(screen.getByText('product brand')).toBeInTheDocument()
    expect(screen.getByText('2 hours ago')).toBeInTheDocument()
    expect(screen.getByText('reviewer name')).toBeInTheDocument()
    expect(screen.getByText('reviewer skin type')).toBeInTheDocument()
})

test('display the correct amount of stars', async () => {
    expect(screen.getAllByTestId('test-review-card-filled-star').length).toBe(3)
    expect(screen.getAllByTestId('test-review-card-empty-star').length).toBe(2)
})
