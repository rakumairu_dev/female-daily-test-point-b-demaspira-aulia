import React from 'react'

interface AdsProps {
    className: string
    content: string | React.ReactNode
}

const Ads = (props: AdsProps) => {
    return (
        <div data-testid="test-ads" className={`ads ${props.className}`}>
            {props.content}
        </div>
    )
}

export default Ads
