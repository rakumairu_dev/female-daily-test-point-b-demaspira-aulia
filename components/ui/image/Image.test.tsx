import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import Image from './Image'


test('component did load', async () => {
    render(
        <Image
            src="https://placeimg.com/300/300/any"
        />
    )

    expect(screen.getByTestId('test-image')).toBeInTheDocument()
})