import Image from 'components/ui/image/Image'
import React from 'react'

interface ArticleCardProps {
    url: string
    img: string
    title: string
    author: string
    publishedAt: string
}

const ArticleCard = (props: ArticleCardProps) => {
    return (
        <div className="w-full mb-4" data-testid="test-article-card">
            <a href={props.url} className="block pt-[51%] relative w-full h-auto mb-6 overflow-hidden rounded-lg">
                <Image src={props.img} alt="article" width="350px" height="180px" className="absolute inset-0 w-full h-full object-cover object-center image-hover" />
            </a>
            <a href={props.url} className="block mb-4">
                <h3 className="text-[22px] font-bold color-transition hover:text-primary leading-tight">
                    {props.title}
                </h3>
            </a>
            <div className="flex flex-row items-center">
                <p className="font-medium text-subtitle-two">
                    {props.author}
                </p>
                <p className="text-subtitle-three">
                    &nbsp;|&nbsp;<span className='text-subtitle-three'>{props.publishedAt}</span>
                </p>
            </div>
        </div>
    )
}

export default ArticleCard
