import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import ArticleCard from './ArticleCard'


test('component did load', async () => {
    render(
        <ArticleCard
            author='demas'
            img='https://placeimg.com/300/300/any'
            publishedAt='2 hours ago'
            title='article title'
            url='#'
        />
    )

    expect(screen.getByTestId('test-article-card')).toBeInTheDocument()
    expect(screen.getByText('demas')).toBeInTheDocument()
    expect(screen.getByText('2 hours ago')).toBeInTheDocument()
    expect(screen.getByText('article title')).toBeInTheDocument()
})
