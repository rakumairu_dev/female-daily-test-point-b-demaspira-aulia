import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit'
import editorsChoiceSlice from 'features/homepage/editorsChoiceSlice'
import latestArticlesSlice from 'features/homepage/latestArticlesSlice'
import latestReviewSlice from 'features/homepage/latestReview'
import { createWrapper } from 'next-redux-wrapper'

const makeStore = () => configureStore({
    reducer: {
        [editorsChoiceSlice.name]: editorsChoiceSlice.reducer,
        [latestArticlesSlice.name]: latestArticlesSlice.reducer,
        [latestReviewSlice.name]: latestReviewSlice.reducer,
    },
    devTools: true,
})

export type AppStore = ReturnType<typeof makeStore>
export type AppState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, AppState, unknown, Action>;

export const wrapper = createWrapper<AppStore>(makeStore)