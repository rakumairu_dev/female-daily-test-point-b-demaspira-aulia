import React, { useMemo } from 'react'
import { arrayFromNumber } from 'utils/helper'
import Image from '../image/Image'

export interface ProductCardProps {
    editor?: {
        img: string
        name: string
        position: string
    }
    url: string
    brandUrl: string
    img: string
    star: number
    review: number
    title: string
    brand: string
    color?: string
    matchSkinType?: boolean
    withBorder?: boolean
    paddingImage?: boolean
    containerClassName?: string
    isEager?: boolean
}

const ProductCard = (props: ProductCardProps) => {
    const renderStar = useMemo(() => {
        const roundedDownStarAmount = Math.floor(props.star)
        const inActiveStarAmount = 5 - roundedDownStarAmount

        return (
            <>
                {
                    arrayFromNumber(roundedDownStarAmount).map((item, index) =>
                        <Image data-testid="test-product-card-filled-star" key={index} src="./img/star-filled.png" alt="star-filled" width="16px" height="16px" className="w-4 h-4" />
                    )
                }
                {
                    arrayFromNumber(inActiveStarAmount).map((item, index) =>
                        <Image data-testid="test-product-card-empty-star" key={index} src="./img/star.png" alt="star" width="16px" height="16px" className="w-4 h-4" />
                    )
                }
            </>
        )
    }, [props.star])

    return (
        <div data-testid="test-product-card" className={`w-full flex flex-col ${props.containerClassName || ''}`}>
            {
                props.editor &&
                <div className="w-full flex flex-row items-center transform translate-y-3 pl-4 z-1 relative">
                    <Image loading={props.isEager ? 'eager' : 'lazy'} src={props.editor.img} alt="editor" width="60px" height="60px" className="w-[60px] h-[60px] rounded-full mr-3" />
                    <div className="flex flex-col items-start">
                        <p className="font-bold">{props.editor.name}</p>
                        <p className="text-subtitle text-xs mb-2">{props.editor.position}</p>
                    </div>
                </div>
            }
            <div data-testid="test-product-card-content" className={`bg-white ${props.withBorder ? 'border border-border-two' : ''} rounded-lg overflow-hidden h-full hover:shadow-lg shadow-transition`}>
                <div className={`${props.paddingImage ? 'p-6' : ''}`}>
                    <a href={props.url} className="relative w-full h-auto pt-[95%] block">
                        <Image loading={props.isEager ? 'eager' : 'lazy'} src={props.img} alt="product" width="196px" height="187px" className="absolute inset-0 w-full h-full object-cover object-center image-hover" />
                    </a>
                </div>
                <div className="px-4 pb-4">
                    {
                        props.matchSkinType &&
                        <p className="text-primary text-sm font-bold mb-2">
                            Match Skin Type
                        </p>
                    }
                    <div className="flex flex-row items-center mb-4">
                        <p className="font-bold mr-2">{props.star}</p>
                        {renderStar}
                        <p className="text-sm ml-2 font-medium">({props.review})</p>
                    </div>
                    <a href={props.url} className="font-bold mb-1.5 color-transition hover:text-primary block leading-tight">
                        {props.title}
                    </a>
                    <a href={props.brandUrl} className="leading-tight mb-1.5 font-medium color-transition hover:text-primary block line-clamp-2">
                        {props.brand}
                    </a>
                    {
                        props.color &&
                        <p className="text-subtitle leading-tight">
                            {props.color}
                        </p>
                    }
                </div>
            </div>
        </div>
    )
}

export default ProductCard
