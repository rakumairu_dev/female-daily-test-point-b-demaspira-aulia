import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import Ads from './Ads'


test('component did load', async () => {
    render(
        <Ads
            content="ADS 720x520"
            className='classname'
        />
    )

    const renderedEl = screen.getByTestId('test-ads') as HTMLDivElement

    expect(renderedEl).toBeInTheDocument()
    expect(screen.getByText('ADS 720x520')).toBeInTheDocument()
    expect(renderedEl.className.includes('classname')).toBeTruthy()
})
