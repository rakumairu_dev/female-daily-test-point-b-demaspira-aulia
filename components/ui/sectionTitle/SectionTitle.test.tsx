import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import SectionTitle from './SectionTitle'


test('component did load', async () => {
    render(
        <SectionTitle
            title='title'
            subtitle='subtitle'
        />
    )

    expect(screen.getByTestId('test-section-title-h2')).toBeInTheDocument()
    expect(screen.getByTestId('test-section-title-sub')).toBeInTheDocument()
    expect(screen.queryByTestId('test-section-title-see-more')).toBeNull()
})

test('component render see more button', async () => {
    render(
        <SectionTitle
            title='title'
            subtitle='subtitle'
            withSeeMore
            seeMoreUrl='#'
        />
    )

    expect(screen.getByTestId('test-section-title-see-more')).toBeInTheDocument()
})
