import Image from 'components/ui/image/Image'
import React from 'react'


const Nav = () => {
    return (
        <nav className="bg-white w-full">
            <div className="h-[60px] w-full border-b-2 border-border">
                <div className="h-full w-full max-w-[1440px] flex flex-row items-center mx-auto">
                    <div className="flex flex-col items-center justify-center px-5">
                        <span className="h-[2px] bg-black w-5 mb-1"></span>
                        <span className="h-[2px] bg-black w-5 mb-1"></span>
                        <span className="h-[2px] bg-black w-5"></span>
                    </div>
                    <a href="/" className="mr-8">
                        <Image loading='eager' src="./img/fd_logo.png" alt="logo" className="mr-8 h-full flex-shrink-0" />
                    </a>
                    <div className="rounded border boder-border w-full h-[36px] flex flex-row items-center mr-[30px] focus-within:border-primary hover:shadow focus-within:shadow overflow-hidden all-transition">
                        <Image loading='eager' src="./img/search_icon.png" width="32px" height="32px" alt="search icon" className="pl-3" />
                        <input type="text" className="w-full h-full px-3 text-sm focus:outline-none" placeholder="Search products, articles, topics, brands, etc" />
                    </div>
                    <button className="w-[222px] h-full bg-primary flex flex-row items-center justify-center flex-shrink-0 color-transition hover:bg-primary-dark">
                        <Image loading='eager' src="./img/user_icon.png" width="21px" height="21px" alt="search icon" className="w-[21px] h-[21px] mr-4 flex-shrink-0" />
                        <p className="text-white font-light poppins">
                            LOGIN / SIGNUP
                        </p>
                    </button>
                </div>
            </div>
            <div className="h-[42px] w-full flex flex-row items-center justify-center border-b-2 border-border">
                <a href="#" className="link-text mx-4">
                    SKINCARE
                </a>
                <a href="#" className="link-text mx-4">
                    MAKE UP
                </a>
                <a href="#" className="link-text mx-4">
                    BODY
                </a>
                <a href="#" className="link-text mx-4">
                    HAIR
                </a>
                <a href="#" className="link-text mx-4">
                    FRAGRANCE
                </a>
                <a href="#" className="link-text mx-4">
                    NAILS
                </a>
                <a href="#" className="link-text mx-4">
                    TOOLS
                </a>
                <a href="#" className="link-text mx-4">
                    BRANDS
                </a>
            </div>
        </nav>
    )
}

export default Nav
