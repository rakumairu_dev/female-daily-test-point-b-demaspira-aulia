import React from 'react'


const Button = (props: React.ButtonHTMLAttributes<HTMLButtonElement> | React.AnchorHTMLAttributes<HTMLAnchorElement>) => {
    return (
        ("href" in props) ?
            <a
                {...(props as React.AnchorHTMLAttributes<HTMLAnchorElement>)}
                data-testid="test-button"
                className={`h-[44px] bg-primary color-transition hover:bg-primary-dark px-[28px] flex items-center justify-center text-white poppins font-light rounded self-end ${props.className}`}
            >
                {props.children}
            </a>
            :
            <button
                {...(props as React.ButtonHTMLAttributes<HTMLButtonElement>)}
                data-testid="test-button"
                className={`h-[44px] bg-primary color-transition hover:bg-primary-dark px-[28px] flex items-center justify-center text-white poppins font-light rounded self-end ${props.className}`}
            >
                {props.children}
            </button>
    )
}

export default Button
