import React from "react"
import { useAppSelector } from "lib/hooks"
import Layout from "components/layout/Layout"
import Ads from "components/ui/ads/Ads"
import SectionTitle from "components/ui/sectionTitle/SectionTitle"
import Image from "components/ui/image/Image"
import ProductCard from "components/ui/productCard/ProductCard"
import Button from "components/ui/button/Button"
import ArticleCard from "components/ui/articleCard/ArticleCard"
import ReviewCard from "components/ui/reviewCard/ReviewCard"
import GroupCard from "components/ui/groupCard/GroupCard"
import { wrapper } from "lib/store"
import { homepageApi } from "features/homepage/homepageApi"
import { setEditorsChoice } from "features/homepage/editorsChoiceSlice"
import { setLatestArticles } from "features/homepage/latestArticlesSlice"
import { setLatestReview } from "features/homepage/latestReview"
import Carousel from "components/ui/carousel/Carousel"

const Home = () => {
    const {editorsChoice, latestArticles, latestReview} = useAppSelector(state => state)

    return (
        <Layout
            meta={{
                title: 'Female Daily - Info, Artikel, Video dan Review Seputar Kecantikan',
                description: 'Temukan Informasi terkini dan terlengkap seputar dunia kecantikan mulai dari skin care, makeup, review produk dan brand kecantikan lokal maupun internasional.',
            }}
        >
            <div className="mb-16">
                <Ads className="w-[970px] h-[50px] mt-3 mb-[30px]" content="Top Frame 970x50" />
                <Ads className="w-[970px] h-[250px] mb-16" content="Billboard 970x250" />
                <div className="fd-container mb-16">
                    <SectionTitle
                        title="Editor's Choice"
                        subtitle="Curated with love"
                    />
                    <div className="grid grid-cols-5 gap-5">
                        {
                            editorsChoice.items.map((editorChoice, index) =>
                                <ProductCard
                                    key={index}
                                    brandUrl="#"
                                    editor={{
                                        img: './img/editor-choice/editor-1.png',
                                        name: editorChoice.editor,
                                        position: editorChoice.role,
                                    }}
                                    url="#"
                                    img={editorChoice.product.image}
                                    star={editorChoice.product.rating}
                                    review={7}
                                    title={editorChoice.product.name}
                                    brand={editorChoice.product.description}
                                    color="Rosy Beige"
                                    withBorder
                                    paddingImage
                                    isEager
                                />
                            )
                        }
                    </div>
                </div>
                <div className="w-full bg-primary-light py-4 mb-[60px]">
                    <div className="fd-container flex flex-row items-center relative">
                        <Image src="./img/bg-mid-banner.png" alt="banner" width="320px" height="388px" className="absolute left-0 bottom-0 transform translate-y-4 -translate-x-40" />
                        <div className="w-full flex-shrink pl-[131px] pr-[28px] flex flex-col relative z-1">
                            <h2 className="text-[25px] font-bold leading-tight mb-4">
                                Looking for products that are just simply perfect for you?
                            </h2>
                            <p className="text-[19px] font-medium leading-tight mb-10">
                                Here are some products that we believe match your skin, hair, and body! Have we mentioned that they solve your concerns too?
                            </p>
                            <Button href="#">
                                See My Matches
                            </Button>
                        </div>
                        <div className="w-3/5 grid grid-cols-3 gap-[30px] flex-shrink-0">
                            <ProductCard
                                brandUrl="#"
                                url="#"
                                img="./img/looking-for/product-1.png"
                                matchSkinType
                                star={4.9}
                                review={7}
                                title="VAL BY VALERIE THOMAS"
                                brand="Pure Pressed Blush"
                                color="Neutral Rose"
                            />
                            <ProductCard
                                brandUrl="#"
                                url="#"
                                img="./img/looking-for/product-2.png"
                                matchSkinType
                                star={4.9}
                                review={7}
                                title="VAL BY VALERIE THOMAS"
                                brand="Phito Pigment Liquid Serum Foundation"
                                color="Neutral Rose"
                            />
                            <ProductCard
                                brandUrl="#"
                                url="#"
                                img="./img/looking-for/product-3.png"
                                matchSkinType
                                star={4.9}
                                review={7}
                                title="VAL BY VALERIE THOMAS"
                                brand="Pure Pressed Blush"
                                color="Neutral Rose"
                            />
                        </div>
                    </div>
                </div>
                <Ads className="w-[970px] h-[250px] mb-16" content={<>Horizontal 970x250<br />(Internal campaign only)</>} />
                <div className="fd-container mb-12">
                    <SectionTitle
                        title="Latest Articles"
                        subtitle="So you can make better purchase decision"
                        withSeeMore
                        seeMoreUrl="#"
                    />
                    <div className="grid grid-cols-3 gap-8">
                        {
                            latestArticles.items.map((article, index) =>
                                <ArticleCard
                                    key={index}
                                    url="#"
                                    img={article.image}
                                    title={article.title}
                                    author={article.author}
                                    publishedAt={article.published_at}
                                />
                            )
                        }
                    </div>
                </div>
                <div className="fd-container mb-16">
                    <div className="w-2/3">
                        <SectionTitle
                            title="Latest Reviews"
                            subtitle="So you can make better purchase decision"
                            withSeeMore
                            seeMoreUrl="#"
                        />
                    </div>
                    <div className="flex flex-row">
                        <div className="w-2/3 flex-shrink">
                            <Carousel
                                show={2}
                                contentClassName="-mx-[30px]"
                                contentWrapperClassName="pb-20"
                                indicatorContainerClassName="pt-5"
                            >
                                {
                                    latestReview.items.map((review, index) =>
                                        <ReviewCard
                                            key={index}
                                            product={{
                                                url: "#",
                                                brandUrl: "#",
                                                img: review.product.image,
                                                title: review.product.name,
                                                brand: review.product.desc,
                                            }}
                                            star={review.star}
                                            publishedAt="2 hours ago"
                                            content={review.comment}
                                            reviewer={{
                                                img: "./img/reviews/reviewer-1.png",
                                                name: review.user,
                                                skinType: review.profile.join(', '),
                                            }}
                                            readMoreUrl="#"
                                            containerClassName="px-[30px]"
                                        />
                                    )
                                }
                            </Carousel>
                        </div>
                        <div className="w-1/3 flex-shrink-0">
                            <div className="w-[300px] h-[250px] ads">
                                MR 2<br />300x250
                            </div>
                        </div>
                    </div>
                </div>
                <div className="fd-container mb-16">
                    <SectionTitle
                        title="Popular Groups"
                        subtitle="Where the beauty TALK are"
                        withSeeMore
                        seeMoreUrl="#"
                    />
                    <div className="grid grid-cols-4 gap-6">
                        <GroupCard
                            url="#"
                            img="./img/popular-groups/person-1.png"
                            title="Embrace the curl"
                            subtitle="May our curls pop and never stop!"
                        />
                        <GroupCard
                            url="#"
                            img="./img/popular-groups/person-2.png"
                            title="Embrace the curl"
                            subtitle="May our curls pop and never stop!"
                        />
                        <GroupCard
                            url="#"
                            img="./img/popular-groups/person-3.png"
                            title="Embrace the curl"
                            subtitle="May our curls pop and never stop!"
                        />
                        <GroupCard
                            url="#"
                            img="./img/popular-groups/person-4.png"
                            title="Embrace the curl"
                            subtitle="May our curls pop and never stop!"
                        />
                    </div>
                </div>
                <div className="fd-container mb-16">
                    <SectionTitle
                        title="Latest Videos"
                        subtitle="Watch and learn, ladies"
                        withSeeMore
                        seeMoreUrl="#"
                    />
                    <div className="grid grid-cols-3 grid-rows-2 gap-[30px]">
                        <a href="#" className="block shadow-transition hover:shadow-lg col-span-2 row-span-2">
                            <Image src="./img/videos/video-1.png" alt="video" width="730px" height="411px" className="w-full h-full object-cover object-center" />
                        </a>
                        <a href="#" className="block shadow-transition hover:shadow-lg">
                            <Image src="./img/videos/video-2.png" alt="video" width="730px" height="411px" className="w-full h-full object-cover object-center" />
                        </a>
                        <a href="#" className="block shadow-transition hover:shadow-lg">
                            <Image src="./img/videos/video-3.png" alt="video" width="730px" height="411px" className="w-full h-full object-cover object-center" />
                        </a>
                    </div>
                </div>
                <div className="fd-container mb-16">
                    <SectionTitle
                        title="Trending This Week"
                        subtitle="See our weekly most reviewed products"
                        withSeeMore
                        seeMoreUrl="#"
                    />
                    <div className="w-full">
                        <Carousel
                            show={6}
                            contentClassName="-mx-4 px-2.5"
                            contentWrapperClassName="pb-4"
                        >
                            <ProductCard
                                url="#"
                                brandUrl="#"
                                img="./img/looking-for/product-3.png"
                                star={4.9}
                                review={7}
                                title="SKINCEUTICALS"
                                brand="C E Ferulic"
                                containerClassName="px-2.5"
                            />
                            <ProductCard
                                url="#"
                                brandUrl="#"
                                img="./img/looking-for/product-2.png"
                                star={4.9}
                                review={7}
                                title="JUICE BEAUTY"
                                brand="Phyto-Pigments Flawless Serum..."
                                color="Rosy Beige"
                                containerClassName="px-2.5"
                            />
                            <ProductCard
                                url="#"
                                brandUrl="#"
                                img="./img/editor-choice/product-3.png"
                                star={4.9}
                                review={7}
                                title="JUICE BEAUTY"
                                brand="Pure Pressed Blush"
                                color="Neutral Rose"
                                containerClassName="px-2.5"
                            />
                            <ProductCard
                                url="#"
                                brandUrl="#"
                                img="./img/looking-for/product-1.png"
                                star={4.9}
                                review={7}
                                title="VAL BY VALERIE THOMAS"
                                brand="Pure Pressed Blush"
                                color="Neutral Rose"
                                containerClassName="px-2.5"
                            />
                            <ProductCard
                                url="#"
                                brandUrl="#"
                                img="./img/looking-for/product-3.png"
                                star={4.9}
                                review={7}
                                title="SKINCEUTICALS"
                                brand="C E Ferulic"
                                containerClassName="px-2.5"
                            />
                            <ProductCard
                                url="#"
                                brandUrl="#"
                                img="./img/looking-for/product-2.png"
                                star={4.9}
                                review={7}
                                title="JUICE BEAUTY"
                                brand="Phyto-Pigments Flawless Serum..."
                                color="Rosy Beige"
                                containerClassName="px-2.5"
                            />
                            <ProductCard
                                url="#"
                                brandUrl="#"
                                img="./img/looking-for/product-2.png"
                                star={4.9}
                                review={7}
                                title="JUICE BEAUTY"
                                brand="Phyto-Pigments Flawless Serum..."
                                color="Rosy Beige"
                                containerClassName="px-2.5"
                            />
                        </Carousel>
                    </div>
                </div>
                <div className="fd-container">
                    <SectionTitle
                        title="Top Brands"
                        subtitle="We all know and love"
                        withSeeMore
                        seeMoreUrl="#"
                    />
                    <div className="flex flex-row items-center justify-between mb-16">
                        <Image src="./img/brands/nivea.png" alt="nivea" width="98px" height="79px" />
                        <Image src="./img/brands/the-ordinary.png" alt="the ordinary" width="167px" height="66px" />
                        <Image src="./img/brands/the-body-shop.png" alt="the body shop" width="89px" height="73px" />
                        <Image src="./img/brands/sk-2.png" alt="sk II" width="111px" height="68px" />
                        <Image src="./img/brands/maybelline.png" alt="maybelline" width="151px" height="28px" />
                        <Image src="./img/brands/innisfree.png" alt="innisfree" width="90px" height="70px" />
                    </div>
                    <h2 className="text-2xl font-bold tracking-tight poppins mb-2">
                        Female Daily - Find everything you want to know about beauty on Female Daily
                    </h2>
                    <p className="text-xl leading-tight font-medium">
                        Product Reviews, Tips & Tricks, Expert and Consumer Opinions, Beauty Tutorials, Discussions, Beauty Workshops, anything!
                        <br />
                        We are here to be your friendly solution to all things beauty, inside and out!
                    </p>
                </div>
            </div>
        </Layout>
    )
}

export const getStaticProps = wrapper.getStaticProps(
    store => async () => {
        const homepageApiData = await homepageApi()

        store.dispatch(setEditorsChoice(homepageApiData["editor's choice"]))
        store.dispatch(setLatestArticles(homepageApiData["latest articles"]))
        store.dispatch(setLatestReview(homepageApiData["latest review"]))

        return {
            props: {},
        }
    }
)

export default Home