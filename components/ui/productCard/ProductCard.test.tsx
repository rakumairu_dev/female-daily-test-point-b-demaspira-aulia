import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import ProductCard, { ProductCardProps } from './ProductCard'

const defaultRender = (additionalProps?: Partial<ProductCardProps>) => render(
    <ProductCard
        brand='product brand'
        brandUrl='#'
        img='https://placeimg.com/300/300/any'
        review={5}
        star={2}
        title="product title"
        url='#'
        {...additionalProps}
    />
)

test('component did load', async () => {
    defaultRender()

    expect(screen.getByTestId('test-product-card')).toBeInTheDocument()
})

test('display the correct amount of stars', async () => {
    defaultRender()

    expect(screen.getAllByTestId('test-product-card-filled-star').length).toBe(2)
    expect(screen.getAllByTestId('test-product-card-empty-star').length).toBe(3)
})

test('display with match skin', async () => {
    defaultRender({
        matchSkinType: true,
    })

    expect(screen.getByText('Match Skin Type')).toBeInTheDocument()
})

test('display with color', async () => {
    defaultRender({
        color: 'product color',
    })

    expect(screen.getByText('product color')).toBeInTheDocument()
})

test('display with border', async () => {
    defaultRender({
        withBorder: true,
    })

    expect(screen.getByTestId('test-product-card-content').className.includes('border')).toBeTruthy()
})

