import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'

// Define a type for the slice state
export interface latestArticles {
    title: string
    author: string
    image: string
    published_at: string
}

interface latestArticlesState {
    items: latestArticles[]
}

// Define the initial state using that type
const initialState: latestArticlesState = {items: []}

const latestArticlesSlice = createSlice({
    name: 'latestArticles',
    initialState,
    reducers: {
        setLatestArticles: (state, action: PayloadAction<latestArticles[]>) => {
            state.items = action.payload
        },
    },
    extraReducers: {
        [HYDRATE]: (state, action) => {
            return {
                ...state,
                ...action.payload.latestArticles,
            }
        }
    },
})

export const { setLatestArticles } = latestArticlesSlice.actions
export default latestArticlesSlice