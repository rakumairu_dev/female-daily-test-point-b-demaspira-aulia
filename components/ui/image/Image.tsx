import React from 'react'


const Image = (props: React.ImgHTMLAttributes<HTMLImageElement>) => {
    return (
        <img data-testid="test-image" {...props} loading={props.loading || 'lazy'} />
    )
}

export default Image
