import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'

// Define a type for the slice state
export interface latestReview {
    user: string
    profile: string[]
    product: {
        image: string
        name: string
        desc: string
    },
    star: number
    comment: string
}

interface latestReviewState {
    items: latestReview[]
}

// Define the initial state using that type
const initialState: latestReviewState = {items: []}

const latestReviewSlice = createSlice({
    name: 'latestReview',
    initialState,
    reducers: {
        setLatestReview: (state, action: PayloadAction<latestReview[]>) => {
            state.items = action.payload
        },
    },
    extraReducers: {
        [HYDRATE]: (state, action) => {
            return {
                ...state,
                ...action.payload.latestReview,
            }
        }
    },
})

export const { setLatestReview } = latestReviewSlice.actions
export default latestReviewSlice