import Image from 'components/ui/image/Image'
import React from 'react'


const Footer = () => {
    return (
        <footer className="w-full border-t-2 border-border">
            <div className="footer-container pt-8">
                <div className="w-full flex flex-row items-start mb-5">
                    <div className="w-full flex-shrink">
                        <div className="grid grid-cols-3 gap-3.5">
                            <a href="#" className="link-text">
                                About Us
                            </a>
                            <a href="#" className="link-text">
                                Terms & Conditions
                            </a>
                            <a href="#" className="link-text">
                                Awards
                            </a>
                            <a href="#" className="link-text">
                                Feedback
                            </a>
                            <a href="#" className="link-text">
                                Privacy Policy
                            </a>
                            <a href="#" className="link-text">
                                Newsletter
                            </a>
                            <a href="#" className="link-text">
                                Contact
                            </a>
                            <a href="#" className="link-text">
                                Help
                            </a>
                        </div>
                    </div>
                    <div className="w-1/3 flex-shrink-0">
                        <p className="text-sm font-black poppins mb-5">
                            Download Our Mobile App
                        </p>
                        <div className="flex flex-row items-center">
                            <a href="#" className="image-hover mr-6">
                                <Image src="./img/download/appstore.png" alt="app store" width="135px" height="40px" />
                            </a>
                            <a href="#" className="image-hover">
                                <Image src="./img/download/playstore.png" alt="play store" width="135px" height="40px" />
                            </a>
                        </div>
                    </div>
                </div>
                <div className="w-full flex flex-row items-center mb-16">
                    <div className="w-full flex-shrink">
                        <Image src="./img/fd_logo.png" alt="fd logo" className="transform -translate-x-2" />
                        <p className="text-subtitle-two font-medium">
                            Copyright &copy; 2015-2017 Female Daily Network ∙ All rights reserved
                        </p>
                    </div>
                    <div className="w-1/3 flex-shrink-0 flex flex-row items-center">
                        <a href="#" className="image-hover mr-5">
                            <Image src="./img/social/fb.png" alt="facebook" width="32px" height="32px" />
                        </a>
                        <a href="#" className="image-hover mr-5">
                            <Image src="./img/social/twitter.png" alt="twitter" width="32px" height="32px" />
                        </a>
                        <a href="#" className="image-hover mr-5">
                            <Image src="./img/social/ig.png" alt="instagram" width="32px" height="32px" />
                        </a>
                        <a href="#" className="image-hover mr-5">
                            <Image src="./img/social/yt.png" alt="youtube" width="32px" height="32px" />
                        </a>
                    </div>
                </div>
                <div className="w-[970px] h-[50px] ads">
                    Bottom Frame 970x50, 468x60, 320x50
                </div>
            </div>
        </footer>
    )
}

export default Footer
