import api from "lib/api"
import { editorsChoice } from "./editorsChoiceSlice"
import { latestArticles } from "./latestArticlesSlice"
import { latestReview } from "./latestReview"

interface homepageApiData {
    "editor's choice": editorsChoice[]
    "latest articles": latestArticles[]
    "latest review": latestReview[]
}

export const homepageApi = async () => await (await api.get('/wp')).data as homepageApiData