import { wrapper } from 'lib/store'
import { AppProps } from 'next/app'
import { FC } from 'react'
import 'styles/styles.css'

const MyApp: FC<AppProps> = ({ Component, pageProps }) => {
    return <Component {...pageProps} />
}

export default wrapper.withRedux(MyApp)
