import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'

// Define a type for the slice state
export interface editorsChoice {
    editor: string
    role: string
    product: {
        name: string
        rating: number
        description: string
        image: string
    }
}

interface editorsChoiceState {
    items: editorsChoice[]
}

// Define the initial state using that type
const initialState: editorsChoiceState = {items: []}

const editorsChoiceSlice = createSlice({
    name: 'editorsChoice',
    initialState,
    reducers: {
        setEditorsChoice: (state, action: PayloadAction<editorsChoice[]>) => {
            state.items = action.payload
        },
    },
    extraReducers: {
        [HYDRATE]: (state, action) => {
            return {
                ...state,
                ...action.payload.editorsChoice,
            }
        }
    },
})

export const { setEditorsChoice } = editorsChoiceSlice.actions
export default editorsChoiceSlice