/**
 * create an array of undefined with a certain length
 * @param length the array length
 * @returns array of undefined with the specified length
 */
export const arrayFromNumber = (length: number): undefined[] => new Array(length).fill(undefined)